# Statistiche COVID-19

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.
## Install node, angular
## Node

If you are on linux type on terminal`sudo apt install node`.
 If you are on macos type on terminal `brew install node`.
 If are on windows download the installer from https://nodejs.org/en/
## Angular

Type on terminal `npm i -g @angular/cli`. 
 If you are on linux you need to be root `sudo npm i -g @angular/cli`
## Development server

For start you have to install all the packages
run a terminal in the project folder and run `npm i`

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
