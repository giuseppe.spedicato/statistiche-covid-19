import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberStaticsComponent } from './number-statics.component';

describe('NumberStaticsComponent', () => {
  let component: NumberStaticsComponent;
  let fixture: ComponentFixture<NumberStaticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberStaticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberStaticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
