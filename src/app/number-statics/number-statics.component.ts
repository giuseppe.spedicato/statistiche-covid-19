import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-number-statics',
  templateUrl: './number-statics.component.html',
  styleUrls: ['./number-statics.component.css']
})
export class NumberStaticsComponent implements OnInit {

  constructor(
    public service: ServiceService
  ) { }
  ngOnInit(): void {
    this.getInfo();
  }

  countAllCont(arr : Array<any>){
    this.contagiati = 0;
    this.morti = 0;
    this.guariti = 0;
    let a : any;
    for(a of arr){
      if(a.countryEnglishName)
        if(a.countryEnglishName == "Italy"){
          if(a.confirmedCount)
            this.contagiati += a.confirmedCount;
          if(a.deadCount)
            this.morti += a.deadCount;
          if(a.curedCount)
            this.guariti += a.curedCount;
        }
    }
  }
  contagiati : Number;
  morti : Number;
  guariti : Number;
  getInfo(){
    this.service.getLatest().subscribe((res : any) => {
      this.countAllCont(res.results);
    })
  }

}
