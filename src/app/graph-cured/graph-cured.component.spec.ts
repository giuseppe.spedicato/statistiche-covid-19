import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphCuredComponent } from './graph-cured.component';

describe('GraphCuredComponent', () => {
  let component: GraphCuredComponent;
  let fixture: ComponentFixture<GraphCuredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphCuredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphCuredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
