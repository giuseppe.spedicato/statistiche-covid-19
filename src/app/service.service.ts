import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(
    private http: HttpClient
  ) { }

  get() {
    return this.http.get("https://lab.isaaclin.cn/nCoV/api/area?latest=0&province=意大利") as Observable<any>;
  }
  getLatest(){
    return this.http.get("https://lab.isaaclin.cn/nCoV/api/area?latest=1") as Observable<any>;
  }
  changeFormat(data: Array<string>){
    let result: Array<string> = []
    for(let i of data)
      result.push(moment(i).format("DD/MM/YYYY"));
    return result;
  }
  calcDayDifference(){
    let i = 1;
    let startValue = moment("01012020","DDMMYYYY").format();
    let week : Array<string> = [];
    while(true){
      week.push(moment(startValue).format());
      startValue = moment("01012020","DDMMYYYY").add(i * 7,'days').format();
      i++;
      if(Number(moment().format('YYYY')) < Number(moment(startValue).format('YYYY')))
        return week;
      else if(moment().format('YYYY') == moment(startValue).format('YYYY'))
        if(Number(moment().format('MM')) < Number(moment(startValue).format('MM')))
          return week;
        else if(moment().format('MM') == moment(startValue).format('MM'))
          if(Number(moment().format('DD')) < Number(moment(startValue).format('DD')))
            return week;
    }
  }
  colorXaxis(data : Array<any>){
    let result: Array<string> = [];
    for(let i of data)
      result.push("#ffffff");
    return result;
  }
}
