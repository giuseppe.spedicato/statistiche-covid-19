import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphicStaticsComponent } from './graphic-statics.component';

describe('GraphicStaticsComponent', () => {
  let component: GraphicStaticsComponent;
  let fixture: ComponentFixture<GraphicStaticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphicStaticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphicStaticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
