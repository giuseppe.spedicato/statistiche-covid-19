import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NumberStaticsComponent } from './number-statics/number-statics.component';
import { GraphicStaticsComponent } from './graphic-statics/graphic-statics.component';
import { HttpClientModule } from '@angular/common/http';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MomentModule } from 'ngx-moment';
import { GraphDeadComponent } from './graph-dead/graph-dead.component';
import { GraphCuredComponent } from './graph-cured/graph-cured.component';

@NgModule({
  declarations: [
    AppComponent,
    NumberStaticsComponent,
    GraphicStaticsComponent,
    GraphDeadComponent,
    GraphCuredComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgApexchartsModule,
    MomentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
