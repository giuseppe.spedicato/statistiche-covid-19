import { Component, OnInit } from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle
} from "ng-apexcharts";
import * as moment from 'moment';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-graph-dead',
  templateUrl: './graph-dead.component.html',
  styleUrls: ['./graph-dead.component.css']
})
export class GraphDeadComponent implements OnInit {

  public chartContagi: any;

  searchDataForTime(data: Array<any>, week: string){
    let contagi : Number = 0;
    for(let i of data)
      if(i.countryEnglishName)
        if(i.countryEnglishName == "Italy"){
          if(i.updateTime)
            if(Number(moment(week).format('x')) > i.updateTime && i.updateTime < Number(moment(week).subtract(7,'days').format("x")))
              if(i.deadCount)
                contagi += i.deadCount;
        }
    return contagi;
  }
  calcData(week: Array<string>){
    let result : Array<Number> = [];
    this.service.get().subscribe((res : any) => {
      for(let i of week)
        result.push(this.searchDataForTime(res.results,i));
      return result;
    });
    return result;
  }

  constructor(
    private service: ServiceService
  ) {
    this.chartContagi = {
      series: [
        {
          name: "Persone morte",
          data: this.calcData(this.service.calcDayDifference())
        }
      ],
      chart: {
        height: 450,
        type: "area"
      },
      title: {
        text: "Morti da gennaio",
        style:{
          fontFamily: "'Roboto', sans-serif",
          fontWeight:  'bold',
          fontSize: "17px",
          color: 'white'
        }
      },
      xaxis: {
        categories: this.service.changeFormat(this.service.calcDayDifference()),
        labels: {
          style:{
            colors: this.service.colorXaxis(this.service.calcDayDifference())
          }
        }
      },
      theme: {
        palette: 'palette7'
      }
    }
  }
  ngOnInit(): void {
  }

}
