import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphDeadComponent } from './graph-dead.component';

describe('GraphDeadComponent', () => {
  let component: GraphDeadComponent;
  let fixture: ComponentFixture<GraphDeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphDeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphDeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
